﻿using Xunit;
using ForwardQuotes.Parser;

namespace ForwardQuotes.UnitTests
{
    public class Tests
    {
        [Fact]
        public void Test()
        {
            var x = new TestMe();
            Assert.Equal(2, x.OnePlusOne());
        }
    }
}
